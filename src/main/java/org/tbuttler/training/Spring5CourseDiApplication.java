package org.tbuttler.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.tbuttler.training.controller.ConstructorInjectedController;
import org.tbuttler.training.controller.CustomNamedBeanController;
import org.tbuttler.training.controller.FieldInjectedController;
import org.tbuttler.training.controller.PrimaryBeanInjectedController;
import org.tbuttler.training.controller.SetterInjectedController;

@SpringBootApplication
public class Spring5CourseDiApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(Spring5CourseDiApplication.class, args);

		// Injection using the @Primary annotation
		PrimaryBeanInjectedController dCtrl = ctx.getBean(PrimaryBeanInjectedController.class);

		// Injection into constructor, field, or setter method
		ConstructorInjectedController cCtrl = ctx.getBean(ConstructorInjectedController.class);
		FieldInjectedController pCtrl = ctx.getBean(FieldInjectedController.class);
		SetterInjectedController sCtrl = ctx.getBean(SetterInjectedController.class);

		// Injection using a named bean (name contains spaces)
		CustomNamedBeanController nCtrl = ctx.getBean(CustomNamedBeanController.class);

		dCtrl.stateWiring();
		cCtrl.stateWiring();
		pCtrl.stateWiring();
		sCtrl.stateWiring();
		nCtrl.stateWiring();
	}
}
