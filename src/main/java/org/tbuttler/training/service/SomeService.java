package org.tbuttler.training.service;

public interface SomeService {

	/**
	 * Prints out basic information about the wiring in the format
	 * 
	 * 
	 * @param client
	 *            using the service
	 * @param wiringInfo
	 *            how was the service injected?
	 * @param serviceInfo
	 *            how was the service identified?
	 */
	void stateWiring(Class<?> client, String wiringInfo, String serviceInfo);
}
