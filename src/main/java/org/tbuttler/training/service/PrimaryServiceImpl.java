package org.tbuttler.training.service;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
@Primary
public class PrimaryServiceImpl extends DefaultServiceImpl implements SomeService {

}
