package org.tbuttler.training.service;

import org.springframework.stereotype.Service;

@Service(value = "custom bean name with spaces")
public class NamedServiceImpl extends DefaultServiceImpl implements SomeService {

	@Override
	public void stateWiring(Class<?> client, String wiringInfo, String serviceInfo) {
		super.stateWiring(client, wiringInfo, "custom bean name (@Qualified)");
	}

}
