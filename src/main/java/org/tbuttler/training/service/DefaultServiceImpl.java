package org.tbuttler.training.service;

import org.springframework.stereotype.Service;

@Service
public class DefaultServiceImpl implements SomeService {

	@Override
	public void stateWiring(Class<?> client, String wiringInfo, String serviceInfo) {
		String space = "\t ";
		StringBuilder builder = new StringBuilder(client.getSimpleName());
		builder.append(space);
		builder.append("service was injected via ");
		builder.append(wiringInfo);
		builder.append(space);
		builder.append(" and identified by ");
		builder.append(serviceInfo);
		System.out.println(builder.toString());
	}

}
