package org.tbuttler.training.controller;

import org.springframework.stereotype.Controller;
import org.tbuttler.training.service.DefaultServiceImpl;

@Controller
public class TypeMatchInjectedController {

	private DefaultServiceImpl service;

	public TypeMatchInjectedController(DefaultServiceImpl service) {
		super();
		this.service = service;
	};

	public void stateWiring() {
		service.stateWiring(getClass(), "constructor", "type of implementing service");
	}

}
