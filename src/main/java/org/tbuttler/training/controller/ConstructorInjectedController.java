package org.tbuttler.training.controller;

import org.springframework.stereotype.Controller;
import org.tbuttler.training.service.SomeService;

@Controller
public class ConstructorInjectedController {

	private SomeService service;

	// NOTE @Autowire may be ommitted - Spring does it anyway
	public ConstructorInjectedController(SomeService service) {
		this.service = service;
	}

	public void stateWiring() {
		service.stateWiring(getClass(), "constructor", "primary annotation");
	}

}
