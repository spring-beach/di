package org.tbuttler.training.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.tbuttler.training.service.SomeService;

@Controller
public class SetterInjectedController {

	private SomeService service;

	@Autowired
	@Qualifier("defaultServiceImpl")
	public void setGreetingsService(SomeService service) {
		this.service = service;
	}

	public void stateWiring() {
		service.stateWiring(getClass(), "setter method", "@Qualifier (using implementation class name)");
	}
}
