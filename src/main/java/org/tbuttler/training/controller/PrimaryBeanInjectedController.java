package org.tbuttler.training.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.tbuttler.training.service.SomeService;

@Controller
public class PrimaryBeanInjectedController {

	private SomeService service;

	@Autowired
	public PrimaryBeanInjectedController(SomeService service) {
		this.service = service;
	}

	public void stateWiring() {
		service.stateWiring(getClass(), "constructor", "primary bean");
	}

}
