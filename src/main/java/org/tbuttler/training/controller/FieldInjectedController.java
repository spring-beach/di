package org.tbuttler.training.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.tbuttler.training.service.SomeService;

@Controller
public class FieldInjectedController {

	@Autowired
	private SomeService greetings;

	public void stateWiring() {
		greetings.stateWiring(getClass(), "field", "primary bean");
	}

}
