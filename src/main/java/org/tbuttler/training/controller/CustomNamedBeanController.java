package org.tbuttler.training.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.tbuttler.training.service.SomeService;

@Controller
public class CustomNamedBeanController {

	private SomeService service;

	@Autowired
	@Qualifier("custom bean name with spaces")
	public void setService(SomeService service) {
		this.service = service;
	}

	public void stateWiring() {
		service.stateWiring(getClass(), "constructor", "@Qualifier - bean with custom name ");
	}
}
