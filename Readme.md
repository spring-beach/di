# What is it?

Demonstration of dependency injection (in Spring 5 using Spring Boot 2).

# Using Autowiring

* package controller: autowiring of beans of type `SomeService` into components using various autowiring mechanisms
* package service: one service interface (`SomeService`) with 3 implementations

## SomeService implementations

* `PrimaryServiceImpl`: has its default bean name (primaryServiceImpl), and is marked as a primary bean
* `DefaultServiceImpl`: has its default bean name (defaultServiceImpl)
* `NamedServiceImpl`: has a custom bean name (containing spaces for the fun of it)

## Autowiring setup and results

| Controller	| has service injected into | using annotations | Expected match by | Results in autowired class |
|--|--|--|--|--|
|ConstructorInjectedController| constructor | none | type |PrimaryServiceImpl |
|PrimaryBeanInjectedController| constructor | `@Autowire` | type |PrimaryServiceImpl | 
|FieldInjectedController| private field | `@Autowire` | type |PrimaryServiceImpl |
|CustomNamedBeanController| setter | `@Qualifier` using custom bean name for NamedServiceImpl, `@Autowire` | name | NamedServiceImpl|
| SetterInjectedController | setter | `@Qualifier` using standard bean name `@Autowire` | name | DefaultServiceImpl|
| TypeMatchInjectedController| constructor | none | (sub-) type | DefaultServiceImpl|

# Accessing the ApplicationContext

Inject the application context by implementing the appropriate interface (e.g., `ApplicationContextAware`), see  ... .ctxAccess#ApplicationContextAccess.

# What did I learn?

* How to inject the application context
* Bean names are not limited to characters allowed in Java class names